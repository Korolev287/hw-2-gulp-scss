const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const cssmin = require('gulp-cssmin');
const del = require('delete');
const imagemin = require('gulp-imagemin');
const purgecss = require('gulp-purgecss');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();


gulp.task('buildScss', function() {
    return gulp.src('./src/scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('styles.min.css'))
    .pipe(autoprefixer({
        cascade: false
    }))
    .pipe(purgecss({
          content: ['index.html']
        })
      )
    .pipe(cssmin())
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('buildImg', function(){
    return gulp.src('./src/img/*')
      .pipe(imagemin())
      .pipe(gulp.dest('./dist/img'))
  });

gulp.task('clean', function() {
    return del(['dist/'])
})

gulp.task('buildJs', function () {
    return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
          .pipe(browserSync.stream())
          .pipe(gulp.dest('./dist/js'))
  });

  gulp.task('dev', function() {
    browserSync.init({
        server: { baseDir: './' }
    })
    gulp.watch('./src/js/*.js', gulp.series(['buildJs']))
    gulp.watch('./src/scss/*.scss', gulp.series(['buildScss'])).on('change', () => browserSync.reload())
    gulp.watch('index.html').on('change', () => browserSync.reload())
})

gulp.task('build', gulp.series('clean', gulp.parallel('buildImg', 'buildJs', 'buildScss')))